﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimeTest
{
    class Program
    {
        static void Main(string[] args)
        {
            int N = 500;

            //get primes (timed)
            var s = DateTime.Now;

            //UNCOMMENT ALGORITHM
            var primes = Modulus(N);
            //var primes = SE1(N).ToList();
            //var primes = SE2(N);            

            var e = DateTime.Now;
            Console.WriteLine($"Time = {(e - s).TotalMilliseconds} ms");
            Console.ReadLine();

            //print results
            int z = 1;
            foreach (var p in primes.OrderBy(x => x))
            {
                Console.WriteLine($"{z++}th prime = {p}");
                if (z > N) break;
            }

            Console.WriteLine("done");
            Console.ReadLine();
        }

        static List<int> Modulus(int N)
        {
            int limit = Bound(N);
            var primes = Enumerable.Range(2, limit).AsParallel().Where(number => Enumerable.Range(2, (int)Math.Sqrt(number) - 1).All(divisor => number % divisor != 0));

            return primes.ToList();
        }

        static int Bound(int N)
        {
            return Convert.ToInt32(N * (Math.Log(N) + Math.Log(Math.Log(N)) - 0.5));
        }

        //https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
        static IEnumerable<int> SE1(int N)
        {
            int limit = Bound(N);

            //list & init
            var primes = Enumerable.Repeat(1, limit).ToArray();

            //drop multiples
            for (int i = 2; i < limit; i++)
            {
                if (primes[i] == 1)
                {
                    for (int j = i; i * j < limit; j++)
                    {
                        primes[i * j] = 0;
                    }
                }
            }

            //results
            for (int p = 2; p < limit; p++)
            {
                if (primes[p] == 1)
                {
                    yield return p;
                }
            }
        }

        //https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
        //but implemented with LINQ
        static List<int> SE2(int N)
        {
            int limit = Bound(N);

            //list & init
            var primes = Enumerable.Range(2, (int)Math.Sqrt(limit) + 2)
            .Aggregate(Enumerable.Range(2, limit - 1).ToArray(), (p, i) =>
            {
                if (p[i - 2] == 0) return p;
                Parallel.For(2, limit / i + 1, m => { p[i * m - 2] = 0; });
                return p;
            })
            .Where(n => n != 0);

            return primes.ToList();
        }
    }
}
